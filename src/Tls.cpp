#include <talos/Tls.hpp>

#include "TlsMessages.hpp"
#include <caligo/X509Certificate.hpp>
#include <caligo/Truststore.hpp>
#include "TlsEnums.hpp"

#include <variant>
#include <sys/mman.h>
#include <cstdio>
#include <set>
#include <exception>
#include <unordered_map>

#include <bini/writer.hpp>
#include <bini/reader.hpp>

#include <caligo/random.hpp>
#include <caligo/sha2.hpp>
#include <caligo/hkdf.hpp>
#include <caligo/aes.hpp>
#include <caligo/ghash.hpp>
#include <caligo/gcm.hpp>
#include <caligo/x25519.hpp>
#include <caligo/chacha20_poly1305.hpp>

namespace Talos {

void log_key(const char* name, std::span<const uint8_t> key) {
  static FILE* sslkeylog = fopen("/home/pebi/sslkeylog.txt", "wb");
  fprintf(sslkeylog, "%s 0000000000000000000000000000000000000000000000000000000000000000 ", name);
  for (auto c : key) {
    fprintf(sslkeylog, "%02x", c);
  }
  fprintf(sslkeylog, "\n");
  fprintf(sslkeylog, "\n");
  fflush(sslkeylog);
}

struct TlsContext {
  struct DomainNameEntry {
    std::vector<Caligo::x509certificate> certs;
    std::unique_ptr<Caligo::PrivateKey> privatekey;
  };
  void AddIdentity(std::string_view certificatesPem, std::string_view privateKeyPem) {
    auto certs = Caligo::parseCertificatesPem(std::span<const uint8_t>(reinterpret_cast<const uint8_t*>(certificatesPem.data()), certificatesPem.size()));
    if (certs[0].fqdns.empty()) {
      // client cert
      entries[certs[0].subject] = DomainNameEntry{
        Caligo::parseCertificatesPem(std::span<const uint8_t>(reinterpret_cast<const uint8_t*>(certificatesPem.data()), certificatesPem.size())),
        Caligo::parsePrivateKey(std::span<const uint8_t>(reinterpret_cast<const uint8_t*>(privateKeyPem.data()), privateKeyPem.size()), Caligo::DataFormat::Pem)
      };
    } else {
      for (auto& domain : certs[0].fqdns) {
        entries[domain] = DomainNameEntry{
          Caligo::parseCertificatesPem(std::span<const uint8_t>(reinterpret_cast<const uint8_t*>(certificatesPem.data()), certificatesPem.size())),
          Caligo::parsePrivateKey(std::span<const uint8_t>(reinterpret_cast<const uint8_t*>(privateKeyPem.data()), privateKeyPem.size()), Caligo::DataFormat::Pem)
        };
        if (not defaultEntry) {
          defaultEntry = &entries[domain];
        }
      }
    }
  }
  DomainNameEntry& GetEntry(std::string_view domainName) {
    auto it = entries.find(std::string(domainName));
    if (it == entries.end()) {
      return *defaultEntry;
    }
    return it->second;
  }
  std::unordered_map<std::string, DomainNameEntry> entries;
  DomainNameEntry* defaultEntry = nullptr;
};

TlsContextHandle::TlsContextHandle() {
  context = new TlsContext();
}

TlsContextHandle::~TlsContextHandle() {
  delete context;
}

void TlsContextHandle::AddIdentity(std::string_view certificatesPem, std::string_view privateKeyPem) {
  context->AddIdentity(certificatesPem, privateKeyPem);
}

template <typename AEAD, typename Hash>
struct TLS13 {
  using Cipher = typename AEAD::Cipher;
  Caligo::secret<Hash> secret;
  Hash handshake;
  Hash original;
  AEAD s;
  AEAD c;
  TLS13(const std::vector<uint8_t>& sharedSecret, std::vector<uint8_t> handshakeSoFar)
  : secret(Caligo::HKDF_HandshakeSecret<Hash>(sharedSecret))
  , handshake(handshakeSoFar)
  , original(handshakeSoFar)
  , s(secret.template get_key_iv<Cipher>(original.data(), false, true))
  , c(secret.template get_key_iv<Cipher>(original.data(), true, true))
  {
    log_key("CLIENT_HANDSHAKE_TRAFFIC_SECRET", secret.get_traffic_secret(original.data(), true, true));
    log_key("SERVER_HANDSHAKE_TRAFFIC_SECRET", secret.get_traffic_secret(original.data(), false, true));
  }
  std::vector<uint8_t> handshake_hmac(bool client) {
    auto hmac = Caligo::HMAC<Hash>(handshake.data(), secret.get_finished_key(original.data(), client));
    return std::vector<uint8_t>(hmac.begin(), hmac.end());
  }
  std::vector<uint8_t> getHandshakeHash() {
    auto hs = handshake.data();
    return std::vector<uint8_t>(hs.begin(), hs.end());
  }
  void switchToApplicationSecret() {
    secret = HKDF_MasterSecret<Hash>(secret);
    s = secret.template get_key_iv<Cipher>(handshake.data(), false);
    c = secret.template get_key_iv<Cipher>(handshake.data(), true);

    log_key("CLIENT_TRAFFIC_SECRET_0", secret.get_traffic_secret(handshake.data(), true, false));
    log_key("SERVER_TRAFFIC_SECRET_0", secret.get_traffic_secret(handshake.data(), false, false));
  }
  void updateTrafficSecrets() {
    secret = HKDF_UpdateSecret(secret);
    s = secret.template get_key_iv<Cipher>(handshake.data(), false);
    c = secret.template get_key_iv<Cipher>(handshake.data(), true);
  }
  void addHandshakeData(std::span<const uint8_t> data) {
    handshake.add(data);
  }
  bool Decrypt(std::span<uint8_t> ciphertext, const std::span<const uint8_t> aad, std::span<const uint8_t> tag, bool server) {
    return server ? c.Decrypt(ciphertext, aad, tag) : s.Decrypt(ciphertext, aad, tag);
  }
  std::array<uint8_t, 16> Encrypt(std::span<uint8_t> plaintext, const std::span<const uint8_t> aad, bool server) {
    return server ? s.Encrypt(plaintext, aad) : c.Encrypt(plaintext, aad);
  }
};

struct NullCipher {
  std::vector<uint8_t> handshakeSoFar;
  void addHandshakeData(std::span<const uint8_t> data) {
    handshakeSoFar.insert(handshakeSoFar.end(), data.begin(), data.end());
  }
  std::vector<uint8_t> getHandshakeHash() {
    return {};
  }
  std::vector<uint8_t> handshake_hmac(bool) {
    return {};
  }
  void switchToApplicationSecret() {}
  void updateTrafficSecrets() {}
  bool Decrypt(std::span<uint8_t> , const std::span<const uint8_t> , std::span<uint8_t>, bool) {
    return false;
  }
  std::array<uint8_t, 16> Encrypt(std::span<uint8_t> , const std::span<const uint8_t>, bool) {
    return {};
  }
};

struct TlsState {
  TlsContext& context;
  Caligo::ec_value privkey = Caligo::ec_value::random_private_key();
  std::variant<NullCipher, 
        TLS13<Caligo::GCM<Caligo::AES<128>>, Caligo::SHA2<256>>, 
        TLS13<Caligo::GCM<Caligo::AES<256>>, Caligo::SHA2<384>>,
        TLS13<Caligo::ChaCha20_Poly1305, Caligo::SHA2<256>>
    > cipher;
  Caligo::x509certificate remoteCert;
  std::string hostname;
  std::vector<std::string> alpnProtocols;
  Origo::time currentTime;
  size_t maxRecordSize = 16385;
  std::vector<uint8_t> recvbuffer;
  AuthenticationState state = AuthenticationState::ClientNew;
  TlsError error;

  TlsState(TlsContextHandle& handle, Origo::time currentTime, std::vector<std::string> alpnProtocols)
  : context(*handle.context)
  , alpnProtocols(std::move(alpnProtocols))
  , currentTime(currentTime)
  , state(AuthenticationState::ServerNew)
  {
  }

  TlsState(TlsContextHandle& handle, std::string hostname, Origo::time currentTime, std::vector<std::string> alpnProtocols)
  : context(*handle.context)
  , hostname(hostname)
  , alpnProtocols(std::move(alpnProtocols))
  , currentTime(currentTime)
  , state(AuthenticationState::ClientNew)
  {
  }

  AuthenticationState getAuthenticationState() {
    return state;
  }

  TlsError getError() {
    return error;
  }

  void TlsFail(TlsError err) {
    // TODO: send error to other side too, maybe
    printf("%s:%d FAIL DISCONNECTED  %d\n", __FILE__, __LINE__, (int)err);
    state = AuthenticationState::Disconnected; 
    error = err;
  }

  std::vector<uint8_t> handleClientHello(std::span<const uint8_t> message, Caligo::ec_value& privkey) {
    Bini::reader r(message);
    uint8_t handshakeType = r.read8();
    uint32_t size = r.read24be();
    if ((Tls::Handshake)handshakeType != Tls::Handshake::client_hello || size != message.size() - 4) {
      TlsFail(TlsError::unexpected_message);
      return {};
    }

    // Stuff we get from client hello
    std::set<uint16_t> cipherSuites;
    std::set<uint16_t> supportedGroups = { 0x17 };
    std::set<uint16_t> signatureAlgorithms = { };
    std::map<uint8_t, std::span<const uint8_t>> keyshares;
    uint16_t tlsver = r.read16be();
    std::string alpnProtocol;
    if (tlsver != 0x0303) {
      TlsFail(TlsError::protocol_version);
      printf("%s:%d Client with version below 1.2 connected, rejecting\n", __FILE__, __LINE__);
      return {};
    }

    r.get(32); // 'random' data, ignore

    uint8_t sessSize = r.read8();
    std::span<const uint8_t> sessionId(r.get(sessSize));
  
    uint16_t cipherSuiteSize = r.read16be();
    for (size_t n = 0; n < cipherSuiteSize / 2; n++) {
      cipherSuites.insert(r.read16be());
    }

    uint8_t compressions = r.read8();
    uint8_t compressionType = r.read8();
    if (compressions != 1 || compressionType != 0) {
      TlsFail(TlsError::unsupported_extension);
      printf("%s:%d Client indicates some form of compression, rejecting\n", __FILE__, __LINE__);
      return {};
    }
    uint16_t extLength = r.read16be();
    Bini::reader exts = r.get(extLength);
    if (r.fail()) {
      TlsFail(TlsError::decode_error);
      printf("%s:%d Malformed client hello, rejecting\n", __FILE__, __LINE__);
      return {};
    }
    std::vector<std::string> requestedAlpnProtocols;
    while (exts.sizeleft()) {
      uint16_t key = exts.read16be();
      uint16_t size = exts.read16be();
      Bini::reader vals = exts.get(size);
      if (exts.fail()) {
        TlsFail(TlsError::decode_error);
        printf("%s:%d Malformed packet inside clienthello extensions\n", __FILE__, __LINE__);
        return {};
      }
      switch((Tls::Extension)key) {
      case Tls::Extension::key_share: // key share
      {
        size_t bytes = vals.read16be();
        Bini::reader vals2 = vals.get(bytes);
        while (vals2.sizeleft()) {
          uint16_t keytype = vals2.read16be();
          uint16_t keysize = vals2.read16be();
          keyshares[keytype] = vals2.get(keysize);
        }
      }
        break;
      case Tls::Extension::signature_algorithms:
      {
        signatureAlgorithms.clear();
        uint16_t count = vals.read16be();
        for (int n = 0; n < count; n += 2) {
          signatureAlgorithms.insert(vals.read16be());
        }
      }
        break;
      case Tls::Extension::psk_key_exchange_modes:
        break;
      case Tls::Extension::server_name:
      {
        vals.read16be();
        vals.read8();
        size_t stringLength = vals.read16be();
        auto sp = vals.get(stringLength);
        hostname = std::string((const char*)sp.data(), stringLength);
      }
        break;
      case Tls::Extension::supported_groups:
      {
        supportedGroups.clear();
        uint16_t count = vals.read16be();
        for (int n = 0; n < count; n += 2) {
          supportedGroups.insert(vals.read16be());
        }
      }
        break;
      case Tls::Extension::supported_versions: // tls version
        vals.read8();
        tlsver = vals.read16be();
        break;
      case Tls::Extension::application_layer_protocol_negotiation:
      {
        size_t bytes = vals.read16be();
        Bini::reader vals2 = vals.get(bytes);
        while (not vals.empty()) {
          size_t stringLength = vals2.read8();
          auto sp = vals.get(stringLength);
          if (vals.fail()) break;
          requestedAlpnProtocols.push_back(std::string((const char*)sp.data(), (const char*)sp.data() + stringLength));
        }
      }
        break; 
      case Tls::Extension::renegotiation_info:
      case Tls::Extension::ec_point_formats:
      case Tls::Extension::extended_master_secret:
        //irrelevant for TLS1.3+
        break;
      case Tls::Extension::session_ticket:
      case Tls::Extension::encrypted_client_hello:
      case Tls::Extension::status_request:
      case Tls::Extension::delegated_credential:
        break;
      case Tls::Extension::record_size_limit:
      {
        size_t requestedSize = vals.read16be();
        if (requestedSize <= maxRecordSize) {
          maxRecordSize = requestedSize;
        }
      }
        break;
      default: 
        fprintf(stderr, "Found %02X\n", key);
        break;
      }
    }
    
    if (tlsver != 0x0304) {
      TlsFail(TlsError::protocol_version);
      printf("%s:%d Refusing connection not using to TLS 1.3\n", __FILE__, __LINE__);
      return {};
    }
    if (!supportedGroups.contains(0x1D)) {
      TlsFail(TlsError::missing_extension);
      printf("%s:%d No shared supported group found (accepting only x25519)\n", __FILE__, __LINE__);
      return {};
    }
    // Need to check the actual algorithm our certificate(s) use
    /*
    if (!signatureAlgorithms.contains()) {
      TlsFail(TlsError::missing_extension);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
      return;
    }
    */

    auto& keyshare_x25519 = keyshares[0x1D];
    if (keyshare_x25519.size() != 0x20) {
      TlsFail(TlsError::decode_error);
      printf("%s:%d x25519 keyshare of the wrong size\n", __FILE__, __LINE__);
      return {};
    }
    Caligo::ec_value sharedkey = Caligo::X25519(privkey, Caligo::ec_value(keyshare_x25519));
    if (sharedkey == Caligo::ec_value{0}) {
      TlsFail(TlsError::insufficient_security);
      printf("%s:%d x25519 calculation ended in zero key, refusing\n", __FILE__, __LINE__);
      return {};
    }
    std::vector<uint8_t> sharedData = sharedkey.as_bytes();
    sharedkey.wipe();
    if (sharedData.empty()) {
      TlsFail(TlsError::internal_error);
      printf("%s:%d Error computing shared key\n", __FILE__, __LINE__);
      return {};
    }
    std::array<uint16_t, 3> preferredCipherSuites = { 0x1303, 0x1302, 0x1301 };
    uint16_t cipherSuite = 0;
    for (auto& option : preferredCipherSuites) {
      if (cipherSuites.contains(option)) {
        cipherSuite = option;
        break;
      }
    }

    if (cipherSuite == 0) {
      TlsFail(TlsError::insufficient_security);
      printf("%s:%d No supported cipher suite found\n", __FILE__, __LINE__);
      return {};
    }
  
    std::string selectedAlpnProtocol = "";
    // Select the appropriate alpn protocol
    if (not alpnProtocols.empty() and not requestedAlpnProtocols.empty()) {
      for (auto& protocol : requestedAlpnProtocols) {
        if (std::find(alpnProtocols.begin(), alpnProtocols.end(), protocol) != alpnProtocols.end()) {
          selectedAlpnProtocol = protocol;
          alpnProtocols.clear();
          alpnProtocols.push_back(protocol);
          break;
        }
      }
      if (selectedAlpnProtocol.empty()) {
        TlsFail(TlsError::no_application_protocol);
        printf("%s:%d Request with alpn protocols sent, but no matching one found\n", __FILE__, __LINE__);
        return {};
      }
    }

    state = AuthenticationState::WaitingForClientFinished;

    // Now prepare the care package for the client
    // 1. ServerHello
    std::vector<uint8_t> rv = serverHello(cipherSuite, 0x1D, sessionId, Caligo::X25519(privkey, Caligo::bignum<256>(9)).as_bytes());
    privkey.wipe();
    std::visit([&](auto& c){ c.addHandshakeData(std::span<const uint8_t>(rv.data() + 5, rv.size() - 5)); }, cipher);
    std::vector<uint8_t> changeCipherSpec{0x14, 3, 3, 0, 1, 1};
    rv.insert(rv.end(), changeCipherSpec.begin(), changeCipherSpec.end());

    // Include the serverhello into the handshake hash so far
    if (cipherSuite == 0x1303) {
      cipher = TLS13<Caligo::ChaCha20_Poly1305, Caligo::SHA2<256>>(sharedData, std::move(std::get<NullCipher>(cipher).handshakeSoFar));
    } else if (cipherSuite == 0x1302) {
      cipher = TLS13<Caligo::GCM<Caligo::AES<256>>, Caligo::SHA2<384>>(sharedData, std::move(std::get<NullCipher>(cipher).handshakeSoFar));
    } else if (cipherSuite == 0x1301) {
      cipher = TLS13<Caligo::GCM<Caligo::AES<128>>, Caligo::SHA2<256>>(sharedData, std::move(std::get<NullCipher>(cipher).handshakeSoFar));
    }
    // In one encrypted block:
    // 2. EncryptedExtensions
    std::vector<uint8_t> encexts = EncryptedExtensions(selectedAlpnProtocol, maxRecordSize);

    // 3. Certificate
    TlsContext::DomainNameEntry& myName = context.GetEntry(hostname);
    std::vector<uint8_t> cert = Certificate(myName.certs);
    std::vector<uint8_t> hashWithCert = std::visit([&](auto& c){ 
      c.addHandshakeData(encexts); 
      c.addHandshakeData(cert); 
      return c.getHandshakeHash();
    }, cipher);

    // 4. CertificateVerify
    std::vector<uint8_t> certverify = ServerCertificateVerify(signatureAlgorithms, *myName.privatekey, hashWithCert);

    std::vector<uint8_t> hashForFinished = std::visit([&](auto& c){ 
      c.addHandshakeData(certverify); 
      return c.handshake_hmac(false);
    }, cipher);

    // 5. Finished
    std::vector<uint8_t> finished = Finished(hashForFinished);

    encexts.insert(encexts.end(), cert.begin(), cert.end());
    encexts.insert(encexts.end(), certverify.begin(), certverify.end());
    encexts.insert(encexts.end(), finished.begin(), finished.end());

    std::vector<uint8_t> encrypted = encrypt_message(encexts, 0x16);
    rv.insert(rv.end(), encrypted.begin(), encrypted.end());
    std::visit([&](auto& c){ 
      c.addHandshakeData(finished); 
    }, cipher);

    return rv;
  }

  void handleServerHello(std::span<const uint8_t> message, Caligo::ec_value& privkey) {
    Bini::reader r(message);
    uint8_t handshakeType = r.read8();
    uint32_t size = r.read24be();
    if ((Tls::Handshake)handshakeType != Tls::Handshake::server_hello || size != message.size() - 4) {
      TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
      return;
    }

    uint16_t tlsver = r.read16be();
    if (tlsver != 0x0303) {
      TlsFail(TlsError::protocol_version);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
      return;
    }

    std::span<const uint8_t> serverRandom = r.get(32);
    static const std::array<const uint8_t, 32> helloRetryRequest = {
      0xCF, 0x21, 0xAD, 0x74, 0xE5, 0x9A, 0x61, 0x11, 0xBE, 0x1D, 0x8C, 0x02, 0x1E, 0x65, 0xB8, 0x91, 
      0xC2, 0xA2, 0x11, 0x16, 0x7A, 0xBB, 0x8C, 0x5E, 0x07, 0x9E, 0x09, 0xE2, 0xC8, 0xA8, 0x33, 0x9C, 
    };
    if (memcmp(helloRetryRequest.data(), serverRandom.data(), 32) == 0) {
      TlsFail(TlsError::unsupported_extension);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
      return;
      // TODO: Handle this
    }

    uint8_t sessSize = r.read8();
    r.get(sessSize); // ignore session ID; just some more TLS1.2 make pretend

    uint16_t cipherSuite = r.read16be();
    r.read8(); // no compression.
    uint16_t extLength = r.read16be();
    Bini::reader exts = r.get(extLength);
    if (r.fail()) {
      TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
      return;
    }
    while (exts.sizeleft()) {
      uint16_t key = exts.read16be();
      uint16_t size = exts.read16be();
      Bini::reader vals = exts.get(size);
      if (exts.fail()) {
        TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
        return;
      }
      switch((Tls::Extension)key) {
      case Tls::Extension::key_share:
      {
        // yay, we get the server's key too
        uint16_t keytype = vals.read16be();
        uint16_t keysize = vals.read16be();
        if (keytype != 0x1D) {
          TlsFail(TlsError::unsupported_extension);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return;
        }
        if (keysize != 0x20) {
          TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return;
        }
        Caligo::ec_value serverpub = Caligo::ec_value(vals.get(0x20));
        if (vals.fail()) {
          TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return;
        }
        Caligo::ec_value sharedkey = Caligo::X25519(privkey, serverpub);
        privkey.wipe();
        std::vector<uint8_t> sharedData = sharedkey.as_bytes();
        if (sharedkey == Caligo::ec_value{0}) {
          TlsFail(TlsError::insufficient_security);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return;
        }
        sharedkey.wipe();
        switch(cipherSuite) {
        case 0x1301: 
          cipher = TLS13<Caligo::GCM<Caligo::AES<128>>, Caligo::SHA2<256>>(sharedData, std::move(std::get<NullCipher>(cipher).handshakeSoFar));
          break;
        case 0x1302: 
          cipher = TLS13<Caligo::GCM<Caligo::AES<256>>, Caligo::SHA2<384>>(sharedData, std::move(std::get<NullCipher>(cipher).handshakeSoFar));
          break;
        case 0x1303: 
          cipher = TLS13<Caligo::ChaCha20_Poly1305, Caligo::SHA2<256>>(sharedData, std::move(std::get<NullCipher>(cipher).handshakeSoFar));
          break;
        default: 
          TlsFail(TlsError::insufficient_security);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return;
        }
      }
        break;
      case Tls::Extension::supported_versions:
        tlsver = vals.read16be();
        break;
      case Tls::Extension::application_layer_protocol_negotiation:
      {
        size_t bytes = vals.read16be();
        Bini::reader vals2 = vals.get(bytes);
        uint8_t alpnCount = vals2.read8();
        while (alpnProtocols.size() < alpnCount) {
          size_t stringLength = vals2.read8();
          auto sp = vals.get(stringLength);
          alpnProtocols.push_back(std::string((const char*)sp.data(), stringLength));
        }
      }
        break;
      default: 
        fprintf(stderr, "Found %02X\n", key);
        break;
      }
    }
    if (tlsver == 0x0304) {
      state = AuthenticationState::WaitingForEncryptedExtensions;
    } else {
      TlsFail(TlsError::protocol_version);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
    }
  }

  void handleEncryptedExtensions(std::span<const uint8_t> message) {
    (void)message;
    state = AuthenticationState::WaitingForServerCertificate;
    // check for illegal ones or weird ones
  }

  void handleCertificate(std::span<const uint8_t> message) {
    std::vector<Caligo::x509certificate> certs;
    Bini::reader r(message);
    [[maybe_unused]] uint8_t context = r.read8();
    [[maybe_unused]] uint32_t certificateListLength = r.read24be();
    while (r.sizeleft()) {
      uint32_t certlength = r.read24be();
      std::span<const uint8_t> certdata = r.get(certlength);
      certs.push_back(parseCertificate(certdata, Caligo::DataFormat::Der));
      uint16_t extLength = r.read16be();
      r.get(extLength);
    }

    bool isTrustable = Caligo::Truststore::Instance().trust(certs, currentTime);
    if (not isTrustable) {
      TlsFail(TlsError::certificate_unknown);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
    } else if (not certs[0].appliesTo(hostname)) {
      TlsFail(TlsError::bad_certificate);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
    } else {
      remoteCert = std::move(certs[0]);
      state = AuthenticationState::WaitingForServerCertificateVerify;
    }
  }

  void handleCertificateVerify(std::span<const uint8_t> message) {
    if (state != AuthenticationState::WaitingForServerCertificateVerify) {
      TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
      return;
    }

    auto hash = std::visit([&](auto& c) -> std::vector<uint8_t>{ return c.getHandshakeHash(); }, cipher);
    std::vector<uint8_t> tosign;
    std::string tls13_prefix = "                                                                TLS 1.3, server CertificateVerify";
    tls13_prefix.push_back(0); // Do not move to the string literal above; there is *no* char[] constructor in std::string.
    tosign.insert(tosign.end(), tls13_prefix.begin(), tls13_prefix.end());
    tosign.insert(tosign.end(), hash.begin(), hash.end());

    Caligo::Tls13SignatureScheme sigAlgo = (Caligo::Tls13SignatureScheme)((message[0] << 8) + message[1]);
    std::span<const uint8_t> sig = message.subspan(4); // also skip over the size argument

    if (remoteCert.pubkey->validateSignature(sigAlgo, tosign, sig)) {
      state = AuthenticationState::WaitingForServerFinished;
    } else {
      TlsFail(TlsError::handshake_failure);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
    }
  }

  std::vector<uint8_t> handleServerFinished(std::span<const uint8_t> message, std::span<const uint8_t> serverDigest) {
    std::vector<uint8_t> check = std::visit([&](auto& c) { return c.handshake_hmac(false); }, cipher);
    if (check.size() != serverDigest.size() || memcmp(check.data(), serverDigest.data(), check.size()) != 0) {
      TlsFail(TlsError::handshake_failure);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
      return {};
    }

    std::vector<uint8_t> hmac = std::visit([&](auto& c) -> std::vector<uint8_t> { 
      c.addHandshakeData(message); 
      return c.handshake_hmac(true);
    }, cipher);

    std::vector<uint8_t> clientFinished;
    clientFinished.push_back((uint8_t)Tls::Handshake::finished);
    clientFinished.push_back(0);
    clientFinished.push_back(0);
    clientFinished.push_back(hmac.size());
    clientFinished.insert(clientFinished.end(), hmac.begin(), hmac.end());

    std::vector<uint8_t> rv = encrypt_message(clientFinished, 0x16);
    std::visit([&](auto& c) { c.switchToApplicationSecret(); }, cipher);
    state = AuthenticationState::ClientOperational;
    return rv;
  }

  std::vector<uint8_t> handleClientFinished(std::span<const uint8_t> /*message*/, std::span<const uint8_t> serverDigest) {
    std::vector<uint8_t> check = std::visit([&](auto& c) -> std::vector<uint8_t> { 
      return c.handshake_hmac(true);
    }, cipher);
    if (check.size() != serverDigest.size() || memcmp(check.data(), serverDigest.data(), check.size()) != 0) {
      TlsFail(TlsError::handshake_failure);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
      return {};
    }

    std::visit([&](auto& c) { c.switchToApplicationSecret(); }, cipher);
    state = AuthenticationState::ServerOperational;
    return {};
  }

  std::vector<uint8_t> handleStartupMessage(uint16_t messageType, std::span<const uint8_t> message) {
    switch(state) {
      case AuthenticationState::ServerNew:
      {
        std::visit([message = std::span<const uint8_t>(message)](auto& c){ c.addHandshakeData(message); }, cipher);
        if (messageType == 0x16) {
          std::vector<uint8_t> hello = handleClientHello(message, privkey);
          return hello;
        } else {
          TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
        }
      }
        return {};
      case AuthenticationState::WaitingForClientFinished:
      case AuthenticationState::WaitingForServerFinished:
      {
        if (messageType != 0x1716) {
          TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        Bini::reader r(message);
        uint8_t handshakeType = r.read8();
        if ((Tls::Handshake)handshakeType != Tls::Handshake::finished) {
          TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        uint32_t size = r.read24be();
        std::span<const uint8_t> m = r.get(size);
        if (r.fail()) {
          TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        return state == AuthenticationState::WaitingForClientFinished ? handleClientFinished(message, m) : handleServerFinished(message, m);
      }
      case AuthenticationState::ServerOperational:

      case AuthenticationState::ClientOperational:
      case AuthenticationState::Disconnected:
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
        std::terminate();


      case AuthenticationState::ClientNew:
      {
        std::vector<uint8_t> hello = clientHello(hostname, Caligo::X25519(privkey, Caligo::bignum<256>(9)), alpnProtocols);
        std::visit([&](auto& c){ std::span hs = hello; c.addHandshakeData(hs.subspan(5)); }, cipher);
        state = AuthenticationState::WaitingForServerHello;
        return hello;
      }
      case AuthenticationState::WaitingForServerHello:
      {
        std::visit([message = std::span<const uint8_t>(message)](auto& c){ c.addHandshakeData(message); }, cipher);
        if (messageType == 0x16) {
          handleServerHello(message, privkey);
        } else {
          TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
        }
        return {};
      }
      case AuthenticationState::WaitingForEncryptedExtensions:
      {
        if (messageType != 0x1716) {
          TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        std::visit([message = std::span<const uint8_t>(message)](auto& c){ c.addHandshakeData(message); }, cipher);
        Bini::reader r(message);
        uint8_t handshakeType = r.read8();
        if ((Tls::Handshake)handshakeType != Tls::Handshake::encrypted_extensions) {
          TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        uint32_t size = r.read24be();
        std::span<const uint8_t> m = r.get(size);
        if (r.fail()) {
          TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        handleEncryptedExtensions(m);
        return {};
      }
      break;

      case AuthenticationState::WaitingForServerCertificate:
      {
        if (messageType != 0x1716) {
          TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        std::visit([message = std::span<const uint8_t>(message)](auto& c){ c.addHandshakeData(message); }, cipher);
        Bini::reader r(message);
        uint8_t handshakeType = r.read8();
        switch((Tls::Handshake)handshakeType) {
          case Tls::Handshake::certificate_request:
            // TODO: implement certificate request handling for client
            TlsFail(TlsError::unsupported_extension);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
            return {};
          case Tls::Handshake::certificate:
          {
            uint32_t size = r.read24be();
            std::span<const uint8_t> m = r.get(size);
            if (r.fail()) {
              TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
              return {};
            }
            handleCertificate(m);
            return {};
          }
          default:
            TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
            return {};
        }
      }
        break;

      case AuthenticationState::WaitingForServerCertificateVerify:
      {
        if (messageType != 0x1716) {
          TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        Bini::reader r(message);
        uint8_t handshakeType = r.read8();
        if ((Tls::Handshake)handshakeType != Tls::Handshake::certificate_verify) {
          TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        uint32_t size = r.read24be();
        std::span<const uint8_t> m = r.get(size);
        if (r.fail()) {
          TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        handleCertificateVerify(m);
        std::visit([message = std::span<const uint8_t>(message)](auto& c){ c.addHandshakeData(message); }, cipher);
      }
        return {};

    }
  }

  std::vector<uint8_t> startupExchange(std::span<const uint8_t> data) {
    if (state == AuthenticationState::ClientOperational ||
        state == AuthenticationState::ServerOperational ||
        state == AuthenticationState::Disconnected) {
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
      return {};
    }
    // All state changes are triggered by messages, except for the initial message.
    if (state == AuthenticationState::ClientNew) {
      return handleStartupMessage(0, {});
    }
    std::vector<uint8_t> rv;
    recvbuffer.insert(recvbuffer.end(), data.begin(), data.end());
    while (true) {
      Bini::reader r(recvbuffer);
      if (r.sizeleft() <= 5) {
        break;
      }

      uint16_t messageType = r.read8();
      uint16_t tlsver = r.read16be();
      uint16_t size = r.read16be();
      if (tlsver != 0x0303 && 
        not (tlsver == 0x0301 || state == AuthenticationState::ServerNew)) {
        TlsFail(TlsError::protocol_version);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
        return {};
      }
      if (r.sizeleft() < size) break;
      if (messageType == 0x17) {
        std::span<uint8_t> aad = std::span<uint8_t>(recvbuffer).subspan(0, 5);
        std::span<uint8_t> packet = std::span<uint8_t>(recvbuffer).subspan(5, size-16);
        std::span<uint8_t> tag = std::span<uint8_t>(recvbuffer).subspan(5+size-16, 16);
        bool valid = std::visit([&](auto& c){
          return c.Decrypt(packet, aad, tag, state == AuthenticationState::WaitingForClientFinished || state == AuthenticationState::ServerOperational);
        }, cipher);
        if (!valid) {
          TlsFail(TlsError::decrypt_error);
          printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        while (!packet.empty() && packet.back() == 0) packet = packet.subspan(0, packet.size() - 1);
        if (packet.empty()) {
          TlsFail(TlsError::decode_error);
          return {};
        }
        messageType = 0x1700 | packet.back();
        packet = packet.subspan(0, packet.size() - 1);
        Bini::reader dr(packet);
        while (dr.sizeleft() >= 4) {
          Bini::reader r2 = dr;
          r2.read8();
          uint32_t size = r2.read24be();
          std::span<const uint8_t> m = dr.get(size + 4);
          auto feedback = handleStartupMessage(messageType, m);
          rv.insert(rv.end(), feedback.begin(), feedback.end());
        }
      } else if (messageType == 0x14) {
        // Legacy TLS1.2 compat message; ignore
      } else if (messageType == 0x16) {
        std::span<uint8_t> packet = std::span<uint8_t>(recvbuffer).subspan(5, size);
        auto feedback = handleStartupMessage(messageType, packet);
        rv.insert(rv.end(), feedback.begin(), feedback.end());
      } else {
        printf("unknown packet type %hu\n", messageType);
      }
      memmove(recvbuffer.data(), recvbuffer.data() + 5 + size, recvbuffer.size() - size - 5);
      recvbuffer.resize(recvbuffer.size() - size - 5);
    }
    return rv;
  }

  std::vector<uint8_t> encrypt_message(std::span<const uint8_t> msg, uint8_t msgType) {
    std::vector<uint8_t> messages;
    size_t packetCount = (msg.size() + maxRecordSize - 1) / maxRecordSize;
    for (size_t packet = 0; packet < packetCount; packet++) {
      std::span<const uint8_t> packetdata = msg.subspan(packet * maxRecordSize, (packet == packetCount - 1) ? msg.size() - packet * maxRecordSize : maxRecordSize);
      size_t sizeNoHeader = packetdata.size() + 17; // 16 for tag, 1 for msgtype
      std::vector<uint8_t> message;
      message.reserve(packetdata.size() + 22); // 5 byte tls1.2 prefix, 16 byte tag, 1 byte message type
      message.push_back(0x17);
      message.push_back(0x03);
      message.push_back(0x03);
      message.push_back((sizeNoHeader >> 8) & 0xFF);
      message.push_back(sizeNoHeader & 0xFF);
      message.insert(message.end(), packetdata.begin(), packetdata.end());
      message.push_back(msgType);

      auto tag = std::visit([&message, this](auto& c){ return c.Encrypt(std::span<uint8_t>(message).subspan(5), std::span<uint8_t>(message).subspan(0, 5), state == AuthenticationState::WaitingForClientFinished || state == AuthenticationState::ServerOperational);}, cipher);

      message.insert(message.end(), tag.begin(), tag.end());
      messages.insert(messages.end(), message.begin(), message.end());
    }
    return messages;
  }

  // postcondition: a next receive_decode without argument will return nothing
  std::vector<uint8_t> receive_decode(std::span<const uint8_t> data) {
    if (state != AuthenticationState::ClientOperational &&
        state != AuthenticationState::ServerOperational) {
      return {};
    }

    recvbuffer.insert(recvbuffer.end(), data.begin(), data.end());

    std::vector<uint8_t> rv;
    while (true) {
      Bini::reader r(recvbuffer);
      if (r.sizeleft() < 5) {
        return rv;
      }
      uint16_t messageType = r.read8();
      uint16_t tlsver = r.read16be();
      if (tlsver != 0x0303) {
        TlsFail(TlsError::protocol_version);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
        return rv;
      }
      uint16_t size = r.read16be();
      if (r.sizeleft() < size) {
        return rv;
      }
      std::span<uint8_t> aad = std::span<uint8_t>(recvbuffer).subspan(0, 5);
      std::span<uint8_t> message;
      if (messageType == 0x17) {
        std::span<uint8_t> packet = std::span<uint8_t>(recvbuffer).subspan(5, size-16);
        std::span<uint8_t> tag = std::span<uint8_t>(recvbuffer).subspan(5+size-16, 16);
        bool valid = std::visit([&](auto& c){
          return c.Decrypt(packet, aad, tag, state == AuthenticationState::WaitingForClientFinished || state == AuthenticationState::ServerOperational);
        }, cipher);
        if (!valid) {
          TlsFail(TlsError::decrypt_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return rv;
        }
        while (!packet.empty() && packet.back() == 0) packet = packet.subspan(packet.size() - 1);
        if (packet.empty()) {
          TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
          return {};
        }
        messageType = 0x1700 | packet.back();
        message = packet.subspan(0, packet.size() - 1);
      } else {
        message = std::span<uint8_t>(recvbuffer.data() + 5, size);
      }

      switch(messageType) {
        case 0x1717:
          // Data message
          rv.insert(rv.end(), message.begin(), message.end());
          break;
        case 0x1716:
        {
          Bini::reader r(message);
          uint8_t handshakeType = r.read8();

          // Handshake message
          switch((Tls::Handshake)handshakeType) {
          case Tls::Handshake::new_session_ticket:
            break;
          case Tls::Handshake::certificate_request:
          case Tls::Handshake::key_update:
          default:
            // invalid message
            TlsFail(TlsError::unexpected_message);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
            break;
          }
        }
          break;
        case 0x1715:
          if (message.size() < 2) {
            TlsFail(TlsError::decode_error);
      printf("%s:%d DISCONNECTED\n", __FILE__, __LINE__);
            break;
          }

          // Alert
          error = (TlsError)message[1];
          TlsFail((TlsError)error);
          break;
        //   Any records following a Finished message MUST be encrypted under the
        //   appropriate application traffic key as described in Section 7.2.  In
        //   particular, this includes any alerts sent by the server in response
        //   to client Certificate and CertificateVerify messages.
        case 0x14:
        case 0x15:
        case 0x16:
        case 0x17:
        case 0x1714: // This message is invalid in TLS 1.3 per spec
        printf("%s:%d\n", __FILE__, __LINE__);
          TlsFail(TlsError::handshake_failure);
          break;
        default:
        printf("%s:%d\n", __FILE__, __LINE__);
          TlsFail(TlsError::decode_error);
          break;
      }

      memmove(recvbuffer.data(), recvbuffer.data() + 5 + size, recvbuffer.size() - size - 5);
      recvbuffer.resize(recvbuffer.size() - size - 5);
    }
  }

  std::vector<uint8_t> send_encode(std::span<const uint8_t> data) {
    if (state != AuthenticationState::ClientOperational &&
      state != AuthenticationState::ServerOperational) 
      return {};

    return encrypt_message(data, 0x17);
  }
};

static_assert(sizeof(TlsState) < 4096);

std::string to_string(TlsError error) {
  (void)error;
  return "FAIL";
}

namespace {
  template <typename T>
  struct SecureSpace {
    static const size_t allocsize = ((sizeof(T) * 32 + 4095) / 4096) * 4096;
    SecureSpace() {
      base = (char*)mmap(nullptr, allocsize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_LOCKED | MAP_NORESERVE, 0, 0);
      madvise((void*)base, allocsize, MADV_WIPEONFORK | MADV_DONTDUMP);
      mlock2((void*)base, allocsize, MLOCK_ONFAULT);
    }
    ~SecureSpace() {
      if (!base) return;
      if (used) {
        std::terminate();
      }
      munmap((void*)base, allocsize);
    }
    SecureSpace(SecureSpace&& rhs) {
      base = rhs.base;
      used = rhs.used;
      rhs.base = nullptr;
      rhs.used = 0;
    }
    const SecureSpace& operator=(SecureSpace&& rhs) {
      base = rhs.base;
      used = rhs.used;
      rhs.base = nullptr;
      rhs.used = 0;
      return *this;
    }
    volatile char* base = nullptr;
    uint32_t used = 0;
    void* take() {
      for (size_t n = 0; n < 32; n++) {
        if (used & (1 << n)) continue;
        used ^= 1 << n;
        return (void*)(base + n * sizeof(TlsState));
      }
      return nullptr;
    }
    bool put(void* p) {
      char* end = (char*)base + allocsize;
      if (p < base || p > end)
        return false;

      size_t index = ((char*)p - base) / sizeof(TlsState);
      volatile char* p2 = base + index * sizeof(TlsState);
      if (p != (char*)p2 || (used & (1 << index)) == 0)
        std::terminate();
      used ^= 1 << index;
      std::fill(p2, p2 + sizeof(TlsState), 0);
      return true;
    }
  };
  struct TlsStateAllocator {
    std::vector<SecureSpace<TlsState>> locations;
    void* allocate() {
      for (auto& s : locations) {
        if (s.used != 0xFFFFFFFF) {
          return s.take();
        }
      }
      locations.push_back({});
      return locations.back().take();
    }
    void free(void* p) {
      for (auto& s : locations) {
        if (s.put(p)) return;
      }
    }
  } stateAllocator;
}

TlsStateHandle TlsStateHandle::createServer(TlsContextHandle& handle, Origo::time currentTime, std::vector<std::string> supportedAlpnProtocols) {
  return TlsStateHandle{new(stateAllocator.allocate()) TlsState(handle, currentTime, std::move(supportedAlpnProtocols))};
}

TlsStateHandle TlsStateHandle::createClient(TlsContextHandle& handle, std::string hostname, Origo::time currentTime, std::vector<std::string> requestedAlpnProtocols) {
  return TlsStateHandle{new(stateAllocator.allocate()) TlsState(handle, hostname, currentTime, std::move(requestedAlpnProtocols))};
}

TlsStateHandle::~TlsStateHandle() {
  stateAllocator.free(state);
}

std::string TlsStateHandle::getAlpnProtocol() {
  return state->alpnProtocols.empty() ? "" : state->alpnProtocols[0];
}

AuthenticationState TlsStateHandle::getAuthenticationState() {
  return state->getAuthenticationState();
}

TlsError TlsStateHandle::getError() {
  return state->getError();
}

std::vector<uint8_t> TlsStateHandle::startupExchange(std::span<const uint8_t> data) {
  return state->startupExchange(data);
}

std::vector<uint8_t> TlsStateHandle::receive_decode(std::span<const uint8_t> data) {
  return state->receive_decode(data);
}

std::vector<uint8_t> TlsStateHandle::send_encode(std::span<const uint8_t> data) {
  return state->send_encode(data);
}

}

/**
TLS 1.3 flow:
1. Send ClientHello
2. Receive ServerHello
   (if HelloRetryRequest, go to 1)
2X Calculate handshake keys & switch to appropriate cipher
3. Receive EncryptedExtensions
4. (Receive CertificateRequest (if any))
5. Receive Certificate
6. Receive CertificateVerify
7. Receive Finished. ( Send EndOfEarlyData. Only if you sent any.)
8. (Send certificate + certificateverify, if requested.)
9. Send Finished.
9X Calculate application keys
*/

/*
TODO:
rsa_pkcs1_sha256 (certificate signature only)
rsa_pss_rsae_sha256 (certificate signature & certverify)
ecdsa_secp256r1_sha256 (?)

secp256r1 (key exchange)
*/

