#pragma once

#include <caligo/X509Certificate.hpp>
#include "TlsEnums.hpp"

#include <bini/writer.hpp>

#include <caligo/x25519.hpp>

#include <string>
#include <vector>
#include <set>
#include <cstdint>
#include <utility>

namespace Talos {

std::vector<uint8_t> serverHello(uint16_t cipherSuite, uint16_t group, std::span<const uint8_t> sessionId, std::span<const uint8_t> keyshare) {
  Bini::writer header;
  header.add16be(0x0303);
  for (uint8_t n = 0x70; n < 0x90; n++) {
    header.add8(n);
  }
  header.add8((uint8_t)sessionId.size());
  for (auto n : sessionId) {
    header.add8(n);
  }
  header.add16be(cipherSuite);
  header.add8(0);

  Bini::writer extensions;

  // Supported versions
  extensions.add16be(0x2B);
  extensions.add16be(0x02);
  extensions.add16be(0x0304);

  // Key share our key back
  extensions.add16be(0x33);
  extensions.add16be(0x24);
  extensions.add16be(group);
  extensions.add16be(0x20);
  extensions.add(keyshare);

  header.add16be(extensions.size());
  header.add(extensions);

  Bini::writer message;
  message.add8(0x16);
  message.add16be(0x0303);
  message.add16be(header.size() + 4);
  message.add8(0x02);
  message.add24be(header.size());
  message.add(header);
  return std::move(message);
}

std::vector<uint8_t> EncryptedExtensions(std::string_view alpnProtocol, size_t maxRecordSize) {
  Bini::writer ee;
  Bini::writer exts;
  ee.add8(0x8);

  // ALPN
  if (not alpnProtocol.empty()) {
    Bini::writer w;
    w.add8(alpnProtocol.size());
    w.add(alpnProtocol);
    exts.add8(0x10);
    exts.add16be(w.size() + 3);
    exts.add16be(w.size()+1);
    exts.add8(w.size());
    exts.add(w);
  }

  if (maxRecordSize != 16385) {
    exts.add8(0x1C);
    exts.add16be(maxRecordSize);
  }

  ee.add24be(exts.size() + 2);
  ee.add16be(exts.size());
  ee.add(exts);
  return std::move(ee);
}

std::vector<uint8_t> Certificate(std::vector<Caligo::x509certificate> &mycert) {
  Bini::writer certs;
  for (auto &cert : mycert) {
    std::vector<uint8_t> certDer = cert.derCert;
    certs.add24be(certDer.size());
    certs.add(certDer);
    certs.add16be(0); // no extensions
  }
  Bini::writer header;
  header.add8(0x0b);
  header.add24be(certs.size() + 4);
  header.add8(0);
  header.add24be(certs.size());
  header.add(certs);
  return std::move(header);
}

std::vector<uint8_t> ServerCertificateVerify(std::set<uint16_t> allowedAlgorithms, Caligo::PrivateKey& privkey, std::span<const uint8_t> hash) {
  static const std::vector<Caligo::Tls13SignatureScheme> preferredSignatureTypes = {
    Caligo::Tls13SignatureScheme::rsa_pss_rsae_sha256,
    Caligo::Tls13SignatureScheme::rsa_pss_rsae_sha384,
    Caligo::Tls13SignatureScheme::rsa_pss_rsae_sha512,
    Caligo::Tls13SignatureScheme::rsa_pss_pss_sha256,
    Caligo::Tls13SignatureScheme::rsa_pss_pss_sha384,
    Caligo::Tls13SignatureScheme::rsa_pss_pss_sha512,
    Caligo::Tls13SignatureScheme::rsa_pkcs1_sha256,
    Caligo::Tls13SignatureScheme::rsa_pkcs1_sha384,
    Caligo::Tls13SignatureScheme::rsa_pkcs1_sha512,
  };
  std::string tls13_prefix = "                                                                TLS 1.3, server CertificateVerify";
  tls13_prefix.push_back(0); // Do not move to the string literal above; there is *no* char[] constructor in std::string.
  std::vector<uint8_t> tosign;
  tosign.insert(tosign.end(), tls13_prefix.begin(), tls13_prefix.end());
  tosign.insert(tosign.end(), hash.begin(), hash.end());

  for (auto& type : preferredSignatureTypes) {
    if (not allowedAlgorithms.contains(type)) continue;

    std::vector<uint8_t> sig = privkey.sign(type, tosign);

    Bini::writer header;
    header.add8(0x0F);
    header.add24be(sig.size() + 4);
    header.add16be(type);
    header.add16be(sig.size());
    header.add(sig);
    return std::move(header);
  }

  return {};
}

std::vector<uint8_t> Finished(std::span<const uint8_t> hmac) {
  std::vector<uint8_t> finished;
  finished.push_back((uint8_t)Tls::Handshake::finished);
  finished.push_back(0);
  finished.push_back(0);
  finished.push_back(hmac.size());
  finished.insert(finished.end(), hmac.begin(), hmac.end());
  return finished;
}

std::vector<uint8_t> clientHello(const std::string& hostname, const Caligo::ec_value& pubkey, const std::vector<std::string>& alpn) {
  Bini::writer header;
  header.add16be(0x0303);
  for (uint8_t n = 0; n < 32; n++) {
    header.add8(n);
  }
  header.add8(32);
  for (uint8_t n = 224; n != 0; n++) {
    header.add8(n);
  }

  Bini::writer suites;
  suites.add16be(0x1301); // aes128sha256
  suites.add16be(0x1302); // aes256sha384
  suites.add16be(0x1303); // poly1305
  header.add16be(suites.size());
  header.add(suites);

  header.add8(0x01);
  header.add8(0x00);

  Bini::writer extensions;

  // Server Name Indication
  extensions.add16be(0x00); // server name
  extensions.add16be(hostname.size() + 5);
  extensions.add16be(hostname.size() + 3);
  extensions.add8(0x00);
  extensions.add16be(hostname.size());
  extensions.add(hostname);

  // Supported groups
  Bini::writer groups;
  groups.add16be(0x1D); // x25519
//  groups.add16be(0x1E); // x448
//  groups.add16be(0x17); // secp256r1
//  groups.add16be(0x18); // secp384r1 ????
//  groups.add16be(0x19); // secp521r1 ????
  extensions.add16be(0x0a);
  extensions.add16be(groups.size() + 2);
  extensions.add16be(groups.size());
  extensions.add(groups);

  // Understood signature algorithms
  Bini::writer sigalgs;
  std::vector<Caligo::Tls13SignatureScheme> supported = {
    Caligo::Tls13SignatureScheme::rsa_pss_rsae_sha256,
    Caligo::Tls13SignatureScheme::rsa_pss_rsae_sha384,
    Caligo::Tls13SignatureScheme::rsa_pss_rsae_sha512,
    Caligo::Tls13SignatureScheme::rsa_pss_pss_sha256,
    Caligo::Tls13SignatureScheme::rsa_pss_pss_sha384,
    Caligo::Tls13SignatureScheme::rsa_pss_pss_sha512,
    Caligo::Tls13SignatureScheme::rsa_pkcs1_sha256,
    Caligo::Tls13SignatureScheme::rsa_pkcs1_sha384,
    Caligo::Tls13SignatureScheme::rsa_pkcs1_sha512,
//    Caligo::Tls13SignatureScheme::ecdsa_secp256r1_sha256,
//    Caligo::Tls13SignatureScheme::ecdsa_secp384r1_sha384,
//    Caligo::Tls13SignatureScheme::ecdsa_secp521r1_sha512,
    Caligo::Tls13SignatureScheme::ed25519,
//    Caligo::Tls13SignatureScheme::ed448,
  };
  for (auto& s : supported) sigalgs.add16be(s);
  
  extensions.add16be(0x0d);
  extensions.add16be(sigalgs.size() + 2);
  extensions.add16be(sigalgs.size());
  extensions.add(sigalgs);

  // Key share our x25519 key
  extensions.add16be(0x33);
  extensions.add16be(0x26);
  extensions.add16be(0x24);
  extensions.add16be(0x1D);
  extensions.add16be(0x20);
  extensions.add(pubkey.as_bytes());

  // PSK key exchange modes
  extensions.add16be(0x2D);
  extensions.add16be(0x02);
  extensions.add8(0x01);
  extensions.add8(0x01);

  // Indicate we do TLS 1.3
  extensions.add16be(0x2B);
  extensions.add16be(0x03);
  extensions.add8(0x02);
  extensions.add16be(0x0304);
  
  // Add ALPN request if we have one
  if (not alpn.empty()) {
    Bini::writer w;
    for (auto& e : alpn) {
      w.add8((uint8_t)e.size());
      w.add(e);
    }
    extensions.add16be(0x10);
    extensions.add16be(w.size() + 2);
    extensions.add16be(w.size());
    extensions.add(w);
  }

  header.add16be(extensions.size());
  header.add(extensions);

  Bini::writer message;
  message.add8(0x16);
  message.add16be(0x0301);
  message.add16be(header.size() + 4);
  message.add8(0x01);
  message.add24be(header.size());
  message.add(header);

  return std::move(message);
}

}


